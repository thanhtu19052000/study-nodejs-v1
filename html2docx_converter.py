import html2docx

def convert_html_to_docx(html_file, docx_file):
    # Convert HTML to DOCX
    html2docx.convert(html_file, docx_file)

if __name__ == "__main__":
    input_html_file = "input.html"  # Replace with the path to your HTML file
    output_docx_file = "output.docx"  # Replace with the desired output DOCX file path
    
    convert_html_to_docx(input_html_file, output_docx_file)
    print(f"HTML converted to DOCX and saved as '{output_docx_file}'")