from pdf2docx import Converter

def pdf_to_docx(pdf_file, docx_file):
    try:
        cv = Converter(pdf_file)
        cv.convert(docx_file, start=0, end=None)
        cv.close()
        print("Conversion successful")
    except Exception as e:
        print(f"Conversion failed: {e}")

if __name__ == "__main__":
    pdf_to_docx("input2.pdf", "output2.docx")