const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Master = new Schema({
    name: String,
    id: Number,
    parentId: Number,
});

const MasterModal = mongoose.model('masters', Master);

module.exports = MasterModal;
