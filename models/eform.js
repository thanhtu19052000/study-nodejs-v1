const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Eform = new Schema({
    data: Array,
    maGiaoDich: String,
    headerFooterFixed: Array,
});

const EformModal = mongoose.model('eforms', Eform);

module.exports = EformModal;
