import sys
import io
from pdf2docx import Converter

pdf_data = sys.stdin.buffer.read()

pdf_file = io.BytesIO(pdf_data)
docx_file = io.BytesIO()

cv = Converter(pdf_file)
cv.convert(docx_file, start=0, end=None)
cv.close()

sys.stdout.buffer.write(docx_file.getvalue())