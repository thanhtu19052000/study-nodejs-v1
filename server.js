const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const morgan = require('morgan')
const AccountRouter = require('./routers/accountRoute');
const AuthRouter = require('./routers/authRoute');
const GenFileRouter = require('./routers/genFileRoute');
const CreateEformRouter = require('./routers/createEform');
const MasterDataRouter = require('./routers/masterDataRoute');

const dotenv = require('dotenv');
const mongoose = require('mongoose');
const cors = require('cors');

const corsOptions = {
    origin: 'http://localhost:3000',
    credentials: true,            //access-control-allow-credentials:true
    optionSuccessStatus: 200
}
dotenv.config();
app.use(express.json({ limit: '50mb' }));
app.use(cors(corsOptions));

// parse application/json
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(morgan("combined"));
app.use('/api/account/', AccountRouter)
app.use('/', AuthRouter)
app.use('/api/GenFile', GenFileRouter)
app.use('/api/createEform', CreateEformRouter)
app.use('/api/master/', MasterDataRouter)



app.get('/', (req, res, next) => {
    res.json('home');
})



const DB_CONNECT = process.env.DB_CONNECT || 'mongodb://127.0.0.1/Study-nodejs';
try {
    mongoose.connect(DB_CONNECT);
    console.log('Connect DB success');
} catch (error) {
    console.log('Fail to connect DB');

}
const port = 4000;
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
