const express = require('express');
const fs = require('fs');
const router = express.Router();
const puppeteer = require('puppeteer');
const { log } = require('console');
const HTMLtoDOCX = require('html-to-docx');
const docx = require('html-docx-js');
const { exec } = require('child_process');

router.post('/', (req, res, next) => {

  try {
    const { htmlString, fileName } = req.body;
    // Write the HTML string to a file
    const filePath = path.join(__dirname, 'your_directory', fileName);
    fs.writeFileSync(filePath, htmlString);

    res.status(200).json({ message: 'HTML file generated successfully' });
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while generating the HTML file' });
  }
});
router.post('/generate-pdf', async (req, res) => {
  try {
    const { htmlBody, htmlHeader, htmlFooter } = req.body;
    // res.status(200).send('pdfBuffer');
    if (!htmlBody) {
      return res.status(400).json({ error: 'Missing required field (htmlString).' });
    }

    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.setContent(htmlBody);

    const pdfOptions = {
      format: 'A4',
      printBackground: true,
      displayHeaderFooter: true,
      headerTemplate: htmlHeader,
      footerTemplate: htmlFooter,
      margin: {
        top: '90px',    // Adjust top margin as needed
        bottom: '120px', // Adjust bottom margin as needed
        left: '32px',   // Điều chỉnh lề trái
        right: '32px',  // Điều chỉnh lề phải
      },
    };

    const pdfBuffer = await page.pdf(pdfOptions);

    await browser.close();

    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', 'inline; filename=generated.pdf');
    res.status(200).send(pdfBuffer);
  } catch (error) {
    console.error('An error occurred:', error);
    res.status(500).json({ error: 'An error occurred while generating the PDF' });
  }
});
router.post('/pip', async (req, res) => {
  try {
    // const { htmlBody, htmlHeader, htmlFooter } = req.body;
    // // res.status(200).send('pdfBuffer');
    // if (!htmlBody) {
    //   return res.status(400).json({ error: 'Missing required field (htmlString).' });
    // }

    // const browser = await puppeteer.launch();
    // const page = await browser.newPage();

    // await page.setContent(htmlBody);

    // const pdfOptions = {
    //   format: 'A4',
    //   printBackground: true,
    //   displayHeaderFooter: true,
    //   headerTemplate: htmlHeader,
    //   footerTemplate: htmlFooter,
    //   margin: {
    //     top: '90px',    // Adjust top margin as needed
    //     bottom: '120px', // Adjust bottom margin as needed
    //     left: '32px',   // Điều chỉnh lề trái
    //     right: '32px',  // Điều chỉnh lề phải
    //   },
    // };

    // const pdfBuffer = await page.pdf(pdfOptions);



    const command = `python pdf2docx_converter.py`;

    exec(command, (error, stdout, stderr) => {
      if (error) {
        console.error(`Error: ${error}`);
        res.status(500).json({ error: "PDF to DOCX conversion failed." });
        return;
      }

      // console.log(`Conversion successful: ${stdout} ${stderr}`);
      res.status(200).json({ message: "PDF to DOCX conversion successful." });
    });
  } catch (error) {
    console.error('An error occurred:', error);
    res.status(500).json({ error: 'An error occurred while generating the PDF' });
  }
});
router.post('/generate-docx', (req, res) => {
  const headerHtml = `
      <div style="text-align: center;">
        <h1>This is a Responsive Header</h1>
        <p>Adapts to different screen sizes when viewed or printed.</p>
      </div>
    `;

  const htmlBody = req.body.htmlBody;

  // Combine header and content HTML
  const combinedHtml = `
      <html>
        <head></head>
        <body>
          ${headerHtml}
          ${htmlBody}
        </body>
      </html>
    `;

  // Convert combined HTML to DOCX
  const docxBuffer = docx.asBlob(combinedHtml);
  res.setHeader('Content-Disposition', `attachment; filename=hi`);
  res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');

  res.status(200).send(docxBuffer);
});

module.exports = router;