const express = require('express');
const fs = require('fs');
const router = express.Router();
const puppeteer = require('puppeteer');
const footerImage = require('../constans/image');
const { log } = require('console');
const EformModal = require('../models/eform');

router.get('/:maGD', (req, res, next) => {
    console.log('day la ma giao dich', req.params.maGD)
    EformModal.findOne({ maGiaoDich: req.params.maGD })
        .then(data => {
            console.log(data);
            res.json(data)
        })
        .catch(err => {
            res.status(500).json('Fail connect Server')
        })
});
router.post('/', (req, res, next) => {
    const dataPayload = req.body.dataPayload;
    const maGiaoDich = req.body.maGiaoDich;
    const headerFooterFixed = req.body.headerFooterFixed
    EformModal.findOne({ maGiaoDich: maGiaoDich })
        .then(data => {

            return EformModal.create({
                data: dataPayload,
                maGiaoDich: maGiaoDich,
                headerFooterFixed: headerFooterFixed,
            })

        })
        .then(data => {
            res.json('Tao thanh cong')
        })
        .catch(err => {
            res.status(500).json('Fail connect Server')
        })

});
module.exports = router;