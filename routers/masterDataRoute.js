const express = require('express');
const fs = require('fs');
const router = express.Router();
const puppeteer = require('puppeteer');
const footerImage = require('../constans/image');
const { log } = require('console');
const MasterModal = require('../models/master');

router.get('/:idGd', (req, res, next) => {
    MasterModal.find({ parentId: req.params.idGd })
        .then(data => {
            // console.log(data);
            res.json(data)
        })
        .catch(err => {
            res.status(500).json('Fail connect Server')
        })
});

module.exports = router;